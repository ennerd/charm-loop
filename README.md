Charm/Loop
==========

A minimal event loop implementation which automatically detects the ReactPHP, Amp and Swoole event loops
and integrates with them. If no event loop is found, it falls back to the built-in PHP `register_shutdown_function()` to create an event loop.


Interface
---------

`Charm\Loop::defer(callable $callback)` is the primary function for adding operations to the event loop.
This function maps directly to the corresponding loop function in ReactPHP, Amp, Swoole or the internal
`register_shutdown_function()` event loop.

`Charm\Loop::onReadable(resource $stream, callable $listener)` will invoke the provided $listener as soon
as *reading* the `$stream` resource will no longer block. The `$listener` callback will not be invoked until
the next iteration of the event loop.

`Charm\Loop::onWritable(resource $stream, callable $listener)` will invoke the provided $listener as soon
as *writing* the `$stream` resource will no longer block. The `$listener` callback will not be invoked until the next iteraton of the event loop.

`Charm\Loop::setTimeout(float $seconds, callable $callback)` will invoke `$callback` after $seconds seconds.

`Charm\Loop::setInterval(float $interval, callable $callback)` will invoke `$callback` every $seconds second.

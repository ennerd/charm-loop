<?php
namespace Charm\Loop;

class SwooleEventLoop extends AbstractEventLoop {

    public function __construct() {

    }

    public static function isRunning(): bool {
        if (!function_exists('swoole_event_add')) {
            return false;
        }
        /**
         * TODO See if we can check that the event loop
         * is actually running, for example by checking the
         * debug_backtrace().
         */
        return true;
    }

    public function defer(callable $callable) {
        \Swoole\Event::defer($callable);
    }

    public function onReadable($stream, $callable): ListenerHandleInterface
    {
        $wrapper = function() use (&$callable, &$cancelled) {
            if ($callable === null) {
                return;
            }
            $callable();
        };
        \Swoole\Event::add($stream, $wrapper, null, SWOOLE_EVENT_READ);
        return new ListenerHandle(function() use (&$callable) {
            $callable = null;
        });
    }

    public function onWritable($stream, $callable): ListenerHandleInterface
    {
        $wrapper = function() use (&$callable, &$cancelled) {
            if ($callable === null) {
                return;
            }
            $callable();
        };
        \Swoole\Event::add($stream, null, $wrapper, \SWOOLE_EVENT_WRITE);
        return new ListenerHandle(function() use (&$callable) {
            $callable = null;
        });
    }

    public function setTimeout(float $delay, callable $listener): ListenerHandleInterface
    {
        $id = \Swoole\Timer::after((int) ($delay * 1000), $listener);
        return new ListenerHandle(function() use ($id) {
            \Swoole\Timer::clear($id);
        });
    }
}

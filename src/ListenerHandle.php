<?php
namespace Charm\Loop;

class ListenerHandle implements ListenerHandleInterface {

    private $cancelFunction;
    private $cancelled = false;

    public function __construct(callable $cancelFunction) {
        $this->cancelFunction = $cancelFunction;
    }

    public function cancel(): void
    {
        if ($this->cancelled) {
            return;
        }
        $this->cancelled = true;
        $cancelFunction = $this->cancelFunction;
        $cancelFunction();
    }
}
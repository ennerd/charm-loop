<?php
namespace Charm\Loop;

interface ListenerHandleInterface {

    /**
     * Stop and remove the event handler.
     *
     * @return void
     */
    public function cancel(): void;
}
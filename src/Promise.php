<?php
namespace Charm\Loop;

use Closure;

class Promise {

    const PENDING = 0;

    const RESOLVED = 1;
    const FULFILLED = self::RESOLVED;

    const REJECTED = 2;
    const FAILED = self::REJECTED;

    public int $state = self::PENDING;
    private mixed $value = null;
    private array $listeners = [];

    public function __construct(callable $resolver = null) {
        if ($resolver !== null) {
            $resolver(
                \Closure::fromCallable([ $this, 'resolve' ]),
                \Closure::fromCallable([ $this, 'reject' ])
            );    
        }
    }

    public function then(?callable $onResolve = null, ?callable $onReject = null): Promise {
        $resultPromise = new self();

        if ($this->state === self::FULFILLED) {
            $this->finalize($resultPromise, $onResolve);
        } elseif ($this->state === self::REJECTED) {
            $this->finalize($resultPromise, $onReject);
        } else {
            $this->listeners[] = [$onResolve, $onReject, $resultPromise];
        }

        return $resultPromise;
    }

    public function otherwise(callable $onReject) {
        return $this->then(null, $onReject);
    }

    /**
     * Resolve this promise
     *
     * @param mixed $result
     * @return void
     */
    public function resolve(mixed $value) {
        if ($this->state !== self::PENDING) {
            throw new Error("Promise is already resolved");
        }
        $this->state = self::FULFILLED;
        $this->value = $value;

        foreach ($this->listeners as $listener) {
            $this->finalize($listener[2], $listener[0]);
        }
        $this->listeners = [];
    }

    /**
     * Reject this promise
     *
     * @param mixed $reason
     * @return void
     */
    public function reject(mixed $value) {
        if ($this->state !== self::PENDING) {
            throw new Error("Promise is already resolved");
        }
        $this->state = self::REJECTED;
        $this->value = $value;

        foreach ($this->listeners as $listener) {
            $this->finalize($listener[2], $listener[1]);
        }
        $this->listeners = [];
    }

    private static function castPromise(mixed $value): ?Promise {
        if (!is_object($value)) {
            return null;
        }
        if ($value instanceof self) {
            return $value;
        }
        if (method_exists($value, 'then')) {
            $promise = new self();
            $value->then(function($result) use ($promise) {
                $promise->resolve($result);
            }, function($reason) use ($promise) {
                $promise->reject($reason);
            });
            return $promise;
        }
        return null;
    }

    private function finalize(Promise $promise, callable $callback): void {
        Loop::defer(function() use ($promise, $callback) {
            try {
                $result = $callback($this->value);
                if (null !== ($newPromise = static::castPromise($result))) {
                    $newPromise->then(
                        Closure::fromCallable([ $promise, 'resolve' ]), 
                        Closure::fromCallable([ $promise, 'reject' ])
                    );
                } else {
                    $promise->resolve($result);
                }
            } catch (\Throwable $e) {
                echo "ERROR: ".$e->getMessage()."\n";
                $promise->reject($e);
            }
        });
    }

}
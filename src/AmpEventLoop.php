<?php
namespace Charm\Loop;

use ReflectionClass;

class AmpEventLoop extends AbstractEventLoop {

    public function __construct() {        
    }

    public static function isRunning(): bool {
        if (!class_exists(\Amp\Loop::class, false)) {
            return false;
        }
        $info = \Amp\Loop::getInfo();
        return $info['running'];
    }

    public function defer(callable $callable) {
        \Amp\Loop::defer($callable);
    }

    public function onReadable($stream, callable $callable): ListenerHandleInterface
    {
        $id = \Amp\Loop::onReadable($stream, $callable);
        return new ListenerHandle(function() use ($id) {
            \Amp\Loop::cancel($id);
        });
    }

    public function onWritable($stream, callable $callable): ListenerHandleInterface
    {
        $id = \Amp\Loop::onWritable($stream, $callable);
        return new ListenerHandle(function() use ($id) {
            \Amp\Loop::cancel($id);
        });
    }

    public function setTimeout(float $delay, callable $listener): ListenerHandleInterface
    {
        $id = \Amp\Loop::delay((int) ($delay * 1000), $listener);
        return new ListenerHandle(function() use ($id) {
            \Amp\Loop::cancel($id);
        });
    }

    public function setInterval(float $interval, callable $listener): ListenerHandleInterface {
        $id = \Amp\Loop::repeat((int) ($interval * 1000), $listener);
        return new ListenerHandle(function() use ($id) {
            \Amp\Loop::cancel($id);
        });
    }

    public function stop(): void {
        \Amp\Loop::stop();
    }
}
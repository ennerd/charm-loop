<?php
namespace Charm\Loop;

interface EventLoopInterface {
    public function __construct();

    /**
     * Is the event loop running?
     */
    public static function isRunning(): bool;

    /**
     * Run $callable on the next tick of the event loop
     *
     * @param callable $callable
     * @return void
     */
    public function defer(callable $callable);

    public function onReadable($stream, callable $callable): ListenerHandleInterface;

    public function onWritable($stream, callable $callable): ListenerHandleInterface;

    public function setTimeout(float $delay, callable $callable): ListenerHandleInterface;

    public function setInterval(float $delay, callable $callable): ListenerHandleInterface;

    public function stop();
}
<?php
namespace Charm\Loop;

use Charm\Util\WeakMap;

/**
 * Extends IsolatedEventLoop because it integrates with it to provide private
 * event loops for the 'Loop::run()' method.
 */
final class Loop {

    public function __construct() {
        throw new Error("Can't instantiate '".self::class."'");
    }

    public static function tick(): void {
        
    }

    /**
     * Run a closure with its own event loop, allowing it to run asynchronous
     * code in both synchronous and asynchronous contexts. The callable will
     * receive a EventLoopInterface object. 
     *
     * @param callable $callable
     * @return void
     */
    public static function run(bool $wait, callable $callable): Promise {
        $promise = new Promise();
        $loop = new IsolatedEventLoop();
        $callable($loop);
        if ($wait) {
            while ($tickFunction = $loop->getNextTickFunction()) {
                $tickFunction();
            }
            $promise->resolve(null);
        } else {
            $ticker = function() use ($loop, &$ticker, $promise) {
                $tickFunction = $loop->getNextTickFunction();
                if ($tickFunction === null) {
                    return;
                }
                $tickFunction();
                if ($loop->hasMoreTickFunctions()) {
                    self::instance($ticker)->defer($ticker);
                } else {
                    $promise->resolve(null);
                }
            };
            $ticker();
        }
        return $promise;
    }

    public static function defer(callable $callable) {
        static::instance($callable)->defer($callable);
    }

    public static function onReadable($stream, $callable): ListenerHandleInterface {
        return static::instance($callable)->onReadable($stream, $callable);
    }

    public static function onWritable($stream, $callable): ListenerHandleInterface {
        return static::instance($callable)->onWritable($stream, $callable);
    }

    public static function setTimeout(float $delay, $callable): ListenerHandleInterface {
        return static::instance($callable)->setTimeout($delay, $callable);
    }

    public static function setInterval(float $interval, $callable): ListenerHandleInterface {
        return static::instance($callable)->setInterval($interval, $callable);
    }

    private static ?EventLoopInterface $loopInstance = null;

    /**
     * Detect the current event loop, and return an instance of EventLoopInterface which
     * integrates with the event loop being used.
     *
     * @return EventLoopInterface
     */
    private static function instance(callable $for): EventLoopInterface {
        if (IsolatedEventLoop::$_instanceCount > 0) {
            foreach (debug_backtrace(false) as $trace) {
                if (isset($trace['args'][0]) && is_object($trace['args'][0]) && $trace['args'][0] instanceof IsolatedEventLoop) {
                    return $trace['args'][0];
                }
            }
        }
        if (self::$loopInstance !== null) {
            return self::$loopInstance;
        }
        foreach ([ReactPHPEventLoop::class, AmpEventLoop::class, SwooleEventLoop::class, NativeEventLoop::class] as $backend) {
            if ($backend::isRunning()) {
                return self::$loopInstance = new $backend();
            }
        }
    }


}

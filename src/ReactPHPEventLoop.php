<?php

namespace Charm\Loop;

use ReflectionClass;

class ReactPHPEventLoop extends AbstractEventLoop
{
    private $loop;

    public function __construct()
    {
        if (!$this->isRunning()) {
            throw new Error("The ReactPHP Event Loop does not seem to be running");
        }
        $this->loop = \React\EventLoop\Loop::get();
    }

    public static function isRunning(): bool {
        if (!\class_exists(\React\EventLoop\Loop::class, false)) {
            return false;
        }
        $rc = new ReflectionClass(\React\EventLoop\Loop::class);
        $rp = $rc->getProperty('instance');
        if ($rp->isInitialized()) {
            $rp->setAccessible(true);
            $loop = $rp->getValue();
            if (is_object($loop)) {
                return true;
            }
        }
    }

    public function defer(callable $callable)
    {
        $this->loop->futureTick($callable);
    }

    public function onReadable($stream, $callable): ListenerHandleInterface
    {
        $this->loop->addReadStream($stream, $callable);
        return new ListenerHandle(function() use ($stream, $callable) {
            $this->loop->removeReadStream($stream, $callable);
        });
    }

    public function onWritable($stream, $callable): ListenerHandleInterface {
        $this->loop->addWriteStream($stream, $callable);
        return new ListenerHandle(function() use ($stream, $callable) {
            $this->loop->removeWriteStream($stream, $callable);
        });
    }

    public function setTimeout(float $delay, callable $listener): ListenerHandleInterface
    {
        $timer = $this->loop->addTimer($delay, $listener);
        return new ListenerHandle(function() use ($timer) {
            $this->loop->cancelTimer($timer);
        });
    }

    public function setInterval(float $interval, callable $listener): ListenerHandleInterface {
        $timer = $this->loop->addPeriodicTimer($interval, $listener);
        return new ListenerHandle(function() use ($timer) {
            $this->loop->cancelTimer($timer);
        });
    }
}

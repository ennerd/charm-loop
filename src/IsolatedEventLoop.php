<?php
namespace Charm\Loop;

use Closure;
use SplQueue;
use Charm\Util\WeakMap;
use ReflectionFunction;

/**
 * An event loop designed to run within another event loop,
 * or stand-alone. This can be used to write asynchronous code
 * that works both synchronously and asynchronously.
 */
class IsolatedEventLoop extends AbstractEventLoop {
    /**
     * @internal
     */
    public static int $_instanceCount = 0;
    private static array $_loopMap = [];
    private $_loopMapIds = [];

    public static function isRunning(): bool {
        return false;
    }

    public function __construct() {
        self::$_instanceCount++;
    }

    public static function getLoop(Closure $callable): ?EventLoopInterface {
        $r = new ReflectionFunction($callable);
        return $r->getClosureThis();
    }

    public function __destruct() {
        foreach ($this->_loopMapIds as $id => $null) {
            unset(self::$_loopMap[$id]);
        }
        self::$_instanceCount--;
        if (sizeof($this->queue) > 0) {
            throw new Error("IsolatedEventLoop destroyed while events exist in queue!");
        }
    }

    private function wrap(callable $callable): Closure {
        $runner = function(IsolatedEventLoop $loop, ...$args) use ($callable) {
            return $callable(...$args);
        };
        $wrapper = function(...$args) use ($runner) {
            return $runner($this, ...$args);
        };
        return $wrapper;
    }

    /**
     * Holds callbacks for the event loop
     */
    private array $queue = [];

    public function defer(callable $callable) {
        $callable = $this->wrap($callable);
        $this->queue[] = $callable;
    }

    public function getNextTickFunction(): ?callable {
        return array_shift($this->queue);
    }

    public function hasMoreTickFunctions(): bool {
        return count($this->queue) > 0;
    }
}
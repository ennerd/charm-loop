<?php
namespace Charm\Loop;

use Charm\Error\HttpCodes;
use Closure;

abstract class AbstractEventLoop implements EventLoopInterface {

    /**
     * If the event loop is requested to stop
     */
    private bool $stopped = false;

    /**
     * @var array<string, object{stream: resource, listeners: array<callable>}
     */
    private array $readableStreamListeners = [];

    /**
     * @var array<string, object{stream: resource, listeners: array<callable>}
     */
    private array $writableStreamListeners = [];

    /**
     * Functions will be invoked on the next iteration before stream_select,
     * and is used for adding stream listeners.
     * 
     * @var array<callable>
     */
    private array $streamListenerQueue = [];

    private bool $tickerEnabled = false;

    /**
     * Run a function on the next iteration. This is the only function that must be implemented
     * to have a fully functional event loop.
     *
     * @param callable $callable
     * @return void
     */
    abstract public function defer(callable $callable);

    public function onReadable($stream, callable $callable): ListenerHandleInterface {
        $this->enableTicker();
        $enqueued = false;
        $this->streamListenerQueue[] = function() use ($stream, $callable, &$enqueued) {
            if ($this->stopped) {
                return;
            }
            if ($enqueued) {
                /**
                 * Trick; if $enqueued is true, it means the event listener have been cancelled,
                 * so we won't add it.
                 */
                return;
            }
            $streamId = (int) $stream;
            if (!isset($this->readableStreamListeners[$streamId])) {
                $this->readableStreamListeners[$streamId] = (object) [
                    'stream' => $stream,
                    'listeners' => [],
                ];
            }
            $this->readableStreamListeners[$streamId]->listeners[] = $callable;
        };
        return new ListenerHandle(function() use ($stream, $callable, &$enqueued) {
            if ($this->stopped) {
                return;
            }
            if (!$enqueued) {
                $enqueued = true;
                return;
            }
            $streamId = (int) $stream;
            if (!isset($this->readableStreamListeners[$streamId])) {
                return;
            }
            foreach ($this->readableStreamListeners[$streamId]->listeners as $key => $listener) {
                if ($listener === $callable) {
                    unset($this->readableStreamListeners[$streamId]->listeners[$key]);
                }
                if (sizeof($this->readableStreamListeners[$streamId]->listeners) === 0) {
                    unset($this->readableStreamListeners[$streamId]);
                }
            }
        });
    }

    public function onWritable($stream, callable $callable): ListenerHandleInterface {
        $this->enableTicker();
        $enqueued = false;
        $this->streamListenerQueue[] = function() use ($stream, $callable, &$enqueued) {
            if ($this->stopped) {
                return;
            }
            if ($enqueued) {
                /**
                 * Trick; if $enqueued is true, it means the event listener have been cancelled,
                 * so we won't add it.
                 */
                return;
            }
            $streamId = (int) $stream;
            if (!isset($this->writableStreamListeners[$streamId])) {
                $this->readableStreamListeners[$streamId] = (object) [
                    'stream' => $stream,
                    'listeners' => [],
                ];
            }
            $this->writableStreamListeners[$streamId]->listeners[] = $callable;
        };
        return new ListenerHandle(function() use ($stream, $callable, &$enqueued) {
            if ($this->stopped) {
                return;
            }
            if (!$enqueued) {
                $enqueued = true;
                return;
            }
            $streamId = (int) $stream;
            if (!isset($this->writableStreamListeners[$streamId])) {
                return;
            }
            foreach ($this->writableStreamListeners[$streamId]->listeners as $key => $listener) {
                if ($listener === $callable) {
                    unset($this->writableStreamListeners[$streamId]->listeners[$key]);
                }
                if (sizeof($this->writableStreamListeners[$streamId]->listeners) === 0) {
                    unset($this->writableStreamListeners[$streamId]);
                }
            }
        });
    }

    public function setTimeout(float $delay, callable $listener): ListenerHandleInterface {
        $cancelled = false;
        $startTime = microtime(true) + $delay;
        $waitingFunction = function() use ($startTime, $listener, &$waitingFunction, &$cancelled) {
            if ($this->stopped) {
                return;
            }
            if ($cancelled) {
                return;
            }
            if (microtime(true) < $startTime) {
                $this->defer($waitingFunction);
            } else {
                $listener();
            }
        };
        $this->defer($waitingFunction);
        return new ListenerHandle(function() use (&$cancelled) {
            $cancelled = true;
        });
    }

    public function setInterval(float $interval, callable $listener): ListenerHandleInterface {
        $cancelled = false;
        $startTime = microtime(true) + $interval;
        $waitingFunction = function() use (&$startTime, $interval, $listener, &$waitingFunction, &$cancelled) {
            if ($this->stopped) {
                return;
            }
            if ($cancelled) {
                return;
            }
            if (microtime(true) < $startTime) {
                $this->defer($waitingFunction);
            } else {
                $startTime += $interval;
                try {
                    $listener();
                    $this->defer($waitingFunction);
                } catch (\Throwable $e) {
                    throw $e;
                }
            }
        };
        $this->defer($waitingFunction);
        return new ListenerHandle(function() use (&$cancelled) {
            $cancelled = true;
        });
    }

    public function stop() {
        $this->stopped = true;
    }

    private function enableTicker(): void {
        if ($this->tickerEnabled) {
            return;
        }
        $this->tickerEnabled = true;
        $this->defer(\Closure::fromCallable([ $this, 'ticker' ]));
    }

    /**
     * Function that checks for readable and writable streams on every tick. Used
     * as a fallback for the selected event loop implementation. Some code Borrowed
     * from Amphp.
     *
     * @return void
     */
    private function ticker(): void {
        $this->tickerEnabled = false;

        foreach ($this->streamListenerQueue as $enqueueFunction) {
            $enqueueFunction();
        }

        $read = [];
        $write = [];
        foreach ($this->readableStreamListeners as $streamId => $description) {
            $read[] = $description->stream;
        }
        foreach ($this->writableStreamListeners as $streamId => $description) {
            $write[] = $description->stream;
        }
        $except = null;
        // Failed connection attempts are indicated via except on Windows
        // @link https://github.com/reactphp/event-loop/blob/8bd064ce23c26c4decf186c2a5a818c9a8209eb0/src/StreamSelectLoop.php#L279-L287
        // @link https://docs.microsoft.com/de-de/windows/win32/api/winsock2/nf-winsock2-select
        if (\DIRECTORY_SEPARATOR === '\\') {
            $except = $write;
        }
        $streamSelectIgnoreResult = false;
        \set_error_handler(function($errno, $message) use (&$streamSelectIgnoreResult) {
            // Casing changed in PHP 8 from 'unable' to 'Unable'
            if (\stripos($message, "stream_select(): unable to select [4]: ") === 0) { // EINTR
                $this->streamSelectIgnoreResult = true;

                return;
            }

            if (\strpos($message, 'FD_SETSIZE') !== false) {
                $message = \str_replace(["\r\n", "\n", "\r"], " ", $message);
                $pattern = '(stream_select\(\): You MUST recompile PHP with a larger value of FD_SETSIZE. It is set to (\d+), but you have descriptors numbered at least as high as (\d+)\.)';

                if (\preg_match($pattern, $message, $match)) {
                    $helpLink = 'https://amphp.org/amp/event-loop/#implementations';

                    $message = 'You have reached the limits of stream_select(). It has a FD_SETSIZE of ' . $match[1]
                        . ', but you have file descriptors numbered at least as high as ' . $match[2] . '. '
                        . "You can install one of the extensions listed on {$helpLink} to support a higher number of "
                        . "concurrent file descriptors. If a large number of open file descriptors is unexpected, you "
                        . "might be leaking file descriptors that aren't closed correctly.";
                }
            }

            throw new \Exception($message, $errno);
        });
        $result = \stream_select($read, $write, $except, 0, 5000);
        \restore_error_handler();
        if ($streamSelectIgnoreResult || $result === 0) {
            return;
        }

        if (!$result) {
            throw new Error("Unknown error during stream_select()");
        }

        $promisesToWaitFor = [];

        foreach ($read as $stream) {
            $streamId = (int) $stream;
            if (!isset($this->readableStreamListeners[$streamId])) {
                continue;
            }

            foreach ($this->readableStreamListeners[$streamId]->listeners as $listener) {
                try {
                    $result = $listener();

                    if ($result === null) {
                        continue;
                    }

                    if (self::isPromise($result)) {
                        $promisesToWaitFor[] = $result;
                    }

                } catch (\Throwable $e) {
                    throw $e;
                }
            }
        }

        if ($except) {
            foreach ($except as $key => $socket) {
                $write[$key] = $socket;
            }
        }

        foreach ($write as $stream) {
            $streamId = (int) $stream;
            if (!isset($this->writableStreamListeners[$streamId])) {
                continue;
            }

            foreach ($this->writableStreamListeners[$streamId]->listeners as $listener) {
                try {
                    $result = $listener();

                    if ($result === null) {
                        continue;
                    }

                    if (self::isPromise($result)) {
                        $promisesToWaitFor[] = $result;
                    }

                } catch (\Throwable $e) {
                    throw $e;
                }
            }
        }

        if (sizeof($this->readableStreamListeners) > 0 || sizeof($this->writableStreamListeners) > 0) {
            $this->enableTicker();
        }
    }
    private static function isPromise($value): bool {
        return is_object($value) && (method_exists($value, 'done') || method_exists($value, 'then' || method_exists($value, 'onResolve')));
    }
}
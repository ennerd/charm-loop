<?php
namespace Charm\Loop;

class NativeEventLoop extends AbstractEventLoop {

    public function __construct() {}

    public static function isRunning(): bool {
        return true;
    }

    public function defer(callable $callable) {
        \register_shutdown_function($callable);
    }
}